/*
 The MIT License (MIT)

 Copyright (c) 2016 Lucas

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
$.fn.tabit = function() {
    if (!this.hasClass("tabs-container")) {
        throw new Error("Please call this on a tab container (missing tabs-container class)");
    }

    var main = this;
    (function(main) {
        var tabs = main.find("ul.tabs");
        if (!tabs || tabs.length === 0) throw new Error("No valid tabs found in main container");
        var contents = main.find(".tab-content[data-tab]");
        if (!tabs || tabs.length === 0) throw new Error("No valid contents found in main container. Please populate tabs BEFORE calling tabit, not after.");

        tabs.find("li[data-trigger]").off("click").on("click", function(e) {
            tabs.find("li[data-trigger]").removeClass("selected");

            var t = $(e.target);

            if (!t.attr("data-trigger")) t = t.parents("li[data-trigger]").first();

            var trigger = t.attr("data-trigger");
            if (trigger === "") return;
            t.addClass("selected");
            main.find(".tab-content[data-tab]").removeClass("active");
            main.find(".tab-content[data-tab='" + trigger + "']").first().addClass("active");
            main.trigger("onTabClicked", require("lodash").assign({}, {trigger, button: e.button, buttons: e.buttons}));
        });

        main.find(".tab-content[data-tab]:not(.active)").removeClass("active");

        if (tabs.find(".selected").length === 0) {
            tabs.find("li[data-trigger]").first().trigger("click", null);
        }
    })(this);

    return this;
};