String.prototype.replacePart = function(start, end, replacement) {
    var temp = this.split("");
    for (var i = start; i < end; ++i) {
        temp[i] = "";
    }
    temp[start] = replacement;
    return temp.join("");
};