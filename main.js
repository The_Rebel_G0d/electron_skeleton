const {app, BrowserWindow, Menu, ipcMain} = require("electron");
require("electron-pug")();

let mainWindow;

app.on("ready", init);
app.on("activate", showMain);
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") app.quit();
});

function showMain() {
    if (mainWindow != null) {
        mainWindow.show();
        return;
    }
    mainWindow = new BrowserWindow({webPreferences: {center: true}});
    mainWindow.setMenu(buildDefaultMenu());
    mainWindow.setMenuBarVisibility(false);

    mainWindow.on("closed", () => mainWindow = null);

    //tray.init(mainWindow);

    mainWindow.loadURL(`file://${__dirname}/www/views/index.pug`);
}
function buildDefaultMenu() {
    return process.env.NODE_ENV == "debug" ? Menu.buildFromTemplate([
            {
                click: (item, focusedWindow) => {
                    if (focusedWindow) focusedWindow.webContents.openDevTools();
                },
                accelerator: "F12"
            },
            {
                click: (item, focusedWindow) => {
                    if (focusedWindow) focusedWindow.webContents.reload();
                },
                accelerator: "CmdOrCtrl+R"
            }
        ]) : null;
}
function init() {
    showMain();
}